#!/bin/bash -eux

#
# matterhorn custom provisioning
#

# support for nfs/NAS
sudo yum install -y nfs-utils
sudo service netfs start
sudo chkconfig netfs on

# create matterhorn user
MH_USER=matterhorn
MH_HOME=/home/${MH_USER}
MH_UID=2122

sudo groupadd --gid ${MH_UID} ${MH_USER}
sudo useradd --uid ${MH_UID} ${MH_USER} -g ${MH_USER}
# passwd tip - todo: use pem at lounch and root/ec2_user
sudo usermod --password abcdefghijklmn ${MH_USER}

# create mh mount point
sudo mkdir -p /home/data/opencast
sudo chown ${MH_USER}:${MH_USER} /home/data/opencast

