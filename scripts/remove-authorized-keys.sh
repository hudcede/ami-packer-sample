#!/bin/bash -eux

# remove authorized-keys, so we can set keypair at launch time
# see https://github.com/mitchellh/packer/issues/1799
sudo rm /root/.ssh/authorized_keys

