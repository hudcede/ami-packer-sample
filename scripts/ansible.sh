#!/bin/bash -eux

# add a ansible user (authorized to be a ansible managed node)
ANSIBLE_USER=ansible
ANSIBLE_HOME=/home/${ANSIBLE_USER}
ANSIBLE_UID=2121
ANSIBLE_USER_PUBLIC_KEY_URL=https://s3.amazonaws.com/fake-s3-bucket-with-ssh-public-key/public-key.pub

# add ansible group, if not present
if ! id -g ${ANSIBLE_USER} >/dev/null 2>&1; then
    sudo groupadd --gid ${ANSIBLE_UID} ${ANSIBLE_USER}
fi

# add ansible user, if not present
if ! id -u ${ANSIBLE_USER} >/dev/null 2>&1; then
    sudo useradd --uid ${ANSIBLE_UID} ${ANSIBLE_USER} -g ${ANSIBLE_USER} -G wheel
fi

# reinstate user ansible groups, if such user already existed
sudo usermod -aG wheel ${ANSIBLE_USER}
sudo usermod -g ${ANSIBLE_USER} ${ANSIBLE_USER}

# passwd tip - todo: use pem at launch time and root/ec2_user
sudo usermod --password abcdefghijklmn ${ANSIBLE_USER}

# sudo powers to ansible user
sudo echo "${ANSIBLE_USER}        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# does not require tty to login
sudo sed -i.bak 's/^Defaults.*requiretty/Defaults !requiretty/g' /etc/sudoers

# installing ansible pub ssh key in authorized_keys
sudo mkdir -pm 700 ${ANSIBLE_HOME}/.ssh
sudo curl "${ANSIBLE_USER_PUBLIC_KEY_URL}" >> ${ANSIBLE_HOME}/.ssh/authorized_keys
sudo chmod 0600 ${ANSIBLE_HOME}/.ssh/authorized_keys
sudo chown -R ${ANSIBLE_USER}:${ANSIBLE_USER} ${ANSIBLE_HOME}/.ssh


