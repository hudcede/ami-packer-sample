#!/bin/bash -eux

# disable selinux
sudo sed -i.bak 's/^SELINUX=.*$/SELINUX=disabled/g' /etc/selinux/config

# disable iptables
sudo service iptables stop
sudo chkconfig iptables off

# install epel repo
sudo rpm -Uvh --force http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm

# yum update
sudo yum -y update

# packages
sudo yum groupinstall -y 'System administration tools'
sudo yum install -y ansible
sudo yum install -y vim-enhanced
sudo yum install -y emacs
sudo yum install -y git
sudo yum install -y ntp
sudo yum install -y libselinux-python

# turn off gss authentication
# RSAAuthentication and PubkeyAuthentication default are 'yes'
sudo sed -i.bak 's/\(\s*\)GSSAPIAuthentication\(.*\)$/#\1GSSAPIAuthentication no/g' /etc/ssh/ssh_config
sudo sed -i.bak 's/\(\s*\)GSSAPIAuthentication\(.*\)$/#\1GSSAPIAuthentication no/g' /etc/ssh/sshd_config

# increase ulimit for max proc and files
cat << EOT >> /etc/security/limits.conf
# increase ulimit for max proc and files
@matterhorn   -   nproc   70000
*             -   nofile  70000
EOT

cat << EOT >> /etc/sysctl.conf
# increase max files open
fs.file-max = 70000
EOT

# disable ipv6
cat << EOT >> /etc/sysctl.conf
# disable ipv6, not using selinux
net.ipv6.conf.all.disable_ipv6=1
net.ipv6.conf.default.disable_ipv6=1
EOT

