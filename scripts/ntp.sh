#!/bin/bash -eux

# set timezone
sudo rm /etc/localtime
sudo cp -p /usr/share/zoneinfo/America/New_York /etc/localtime

# replace current ntp servers with dce
sudo service ntpd stop
sudo sed -i.bak 's/^server.*centos.pool.ntp.org.*$//g' /etc/ntp.conf
sudo cat << EOT >> /etc/ntp.conf
server 127.127.1.0
fudge  127.127.1.0 stratum 10
server 140.247.198.1 burst iburst
restrict 149.247.198.1 mask 255.255.255.255 nomodify notrap noquery
restrict 192.168.0.0 mask 255.255.0.0 nomodify notrap
server 140.247.21.1
EOT

# add ntp server for initial sync
sudo echo "149.247.198.1" >> /etc/ntp/step-tickers

# sync time with ntp server
sudo ntpdate 140.247.198.1

# restart ntpd service
sudo service ntpd start
sudo chkconfig ntpd on

# update /etc/sysconfig/clock
cat /etc/sysconfig/clock |  sed 's/ZONE=.*$/ZONE="America\/New_York"/g' | \
    sed 's/UTC=.*$/UTC=no/g' > /tmp/clock && cp -f /tmp/clock /etc/sysconfig/clock

