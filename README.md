dce custom ami generation for mh nodes
======================================

`this repo is a public sample of packer template for DCE custom ami's . it's not being
updated nor maintained; please do not fork.`

this repo contains [packer][packer] templates and is designed to work
with ansible playbooks in the [dce provision repo][provision]to generate
aws amis, customized for nodes in a matterhorn installation.

prerequisites
----------

- [aws access and secret key][aws_credentials]
- [packer][packer]
- [ansible][ansible]
- [dce provision repo][provision]

on macos you can use [homebrew][homebrew] to install packer and ansible

    local> brew install packer
    local> brew install ansible


how to use
----------

this will launch an ec2 instance in aws, run ansible provision to
drop dce customizations to the instance, and create a new ami from
that customized instance.


    # clone this repo
    local> git clone git@bitbucket.org:hudcede/aws-ami-packer.git

    # clone provision repo
    local> git clone git@bitbucket.org:hudcede/provision.git

    # export needed env vars: AWS_ACCESS_KEY, AWS_SECRET_KEY,
    local> cd aws-ami-packer
    local> cp packer.env naomi.env
    local> vi naomi.env
    ...

    # run packer-ami.sh to check the input
    local> ./packer-ami.sh
    ...

    # ex: to create a build ami
    local> packer-ami.sh --env naomi.env --build


[aws_credentials]: http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSGettingStartedGuide/AWSCredentials.html "aws credentials"
[packer]: https://packer.io/ "packer.io"
[ansible]: http://docs.ansible.com/ "ansible"
[homebrew]: http://brew.sh/ "homebrew"
[provision]: https://bitbucket.org/hudcede/provision "provision"

---eop
