#! /bin/bash

#
# interactive script to generate dce custom ami
#
# requires that these env vars are defined:
#   AWS_ACCESS_KEY: personal aws access key
#   AWS_SECRET_KEY: personal aws secret key
#

# initial values
PROGRAM_NAME=$(basename ${0})

# set source ami-id, centos 6.5 from aws marketplace
export SOURCE_AMI="ami-8997afe0" # (pvm)
#export SOURCE_AMI="ami-c2a818aa" # (hvm)


#
# prints usage of this script
#
usage() {
cat << EOF

usage: packer-ami.sh --env <env_file> [--build|--mh|--mysql|--nfs]
  --env: mandatory; file with exported env var for AWS_ACCESS_KEY, AWS_SECRET_KEY
  --build: optional; if present, builds ami for build server only
  --mh: optional; if present, builds ami for mh node only
  --mysql: optional; if present, builds ami for mysql node only
  --nfs: optional; if present, builds ami for nfs dev server only

  if no option for ami function, builds a base node with minimal dce customizations

EOF
exit 1
}

#
# checks that input dir is a git repo
# get input git repo and sha as env var MY_GIT_REPO, MY_GIT_SHA
#
get_git_sha()
{
    git_dir=${1}
    if [ -z ${git_dir} ]; then
        echo "[FAIL] -- missing directory to get git repo and sha"
        exit 1
    fi

    unset MY_GIT_SHA
    unset MY_GIT_REPO

    echo -n "ensure dir :${git_dir}: is a git repo... "
    if [ -d ${git_dir}/.git ]; then
        export MY_GIT_SHA=`cd ${git_dir} && git rev-parse --verify HEAD`
        export MY_GIT_REPO=`cd ${git_dir} && git ls-remote --get-url origin`
        echo " [ OK ]"
    else
        echo " [FAIL] -- ${git_dir} must be a git repo"
        exit 1
    fi
}


#
# generates json file with variables from env
# proper to be used as a packer var-file
#
packer_variables() {
    var_file=${1}

    # variable file already exists?
    if [ -s ${var_file} ]; then
        echo -n "file ${var_file} exists. replace?[y/N]: "
        read resp
        if [ -n resp -a resp != "y" ]; then
            echo "next time then. bailing out..."
            exit 0
        fi
    fi

    # exit if env var is not set
    set -u

    # generate packer variable.json file
    cat > ${var_file} << EOF
    {
        "ami_git_sha":"${AMI_GIT_SHA}",
        "provision_git_sha":"${PROVISION_GIT_SHA}",
        "source_ami": "${SOURCE_AMI}",
        "aws_access_key": "${AWS_ACCESS_KEY}",
        "aws_secret_key": "${AWS_SECRET_KEY}"
    }
EOF
}


#
# main
#

# check file with environment variables
if [ "${1}" = "--env" ]; then
    if [ ! -f ${2} ]; then
        echo "var file does not exist: ${2}"
        usage
    else
        source ${2}
    fi
else
    echo "missing env var"
    usage
fi

# check function of ami to be built
if [ "${3}" == "--build" ]; then
    export PACKER_TEMPLATE="ami-build.json"
    echo -e "\nbuilding a ami for build server"
elif [ "${3}" == "--mh" ]; then
    export PACKER_TEMPLATE="ami-mh.json"
    echo -e "\nbuilding a ami for mh nodes"
elif [ "${3}" == "--mysql" ]; then
    export PACKER_TEMPLATE="ami-mysql.json"
    echo -e "\nbuilding a ami for mh-mysql node"
elif [ "${3}" == "--nfs" ]; then
    export PACKER_TEMPLATE="ami-nfs.json"
    echo -e "\nbuilding a ami for nfs server"
else
    export PACKER_TEMPLATE="ami-base.json"
    echo -e "\nbuilding a ami for mh-base node"
fi

# define repo and sha for aws-ami-repo
get_git_sha $(pwd)
export AMI_GIT_SHA="${MY_GIT_REPO} at ${MY_GIT_SHA}"

# define repo and sha for provision-repo
get_git_sha "$(pwd)/../provision"
export PROVISION_GIT_SHA="${MY_GIT_REPO} at ${MY_GIT_SHA}"

# put the variables for packer together
rm ./variables.json
packer_variables variables.json

echo
echo "launch instance and build ami..."
packer build \
    -var-file=variables.json \
    "${PACKER_TEMPLATE}"

if [ $? -ne 0 ]; then
    echo "[FAIL]"
    exit 1
fi
echo "[DONE]"

